# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Project 1 Requirements:

*Two Parts:*

1. My Music Mobile App
2. Chapter Questions (Chs. 7, 8)
	
#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's follow-up screen (with images and buttons)
* Screenshot of running application's play and pause user interfaces (with images and buttons)
* Screenshots of skill sets 7, 8, and 9

#### Assignment Screenshots:

*Screenshots of running application's splash and opening screen*:

| Splash screen | Opening screen |
| :---: | :---: |
| ![Screenshot of the app's splash screen](img/splash.PNG) | ![Screenshot of the app's opening screen](img/opening.PNG) |

*Screenshots of running application's playing and pause screen*:

| Playing Screen | Pause Screen |
| :---: | :---: |
| ![Screenshot of the app's playing screen](img/playing.PNG) | ![Screenshot of the app's pause screen](img/pause.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 7 | Skill Set 8 | Skill Set 9 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 7 running](img/MeasurementConversion.PNG) | ![Screenshot of Skill Set 8 running](img/DistanceCalculator.PNG) | ![Screenshot of Skill Set 9 running](img/MultipleSelectionLists.PNG) |