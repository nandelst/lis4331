# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Assignment 2 Requirements:

*Two Parts:*

1. Tip Calculator Mobile App
2. Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Screenshots of running application's unpopulated and populated user interface
* Screenshots of skill sets 1, 2, and 3

#### Assignment Screenshots:

*Screenshots of Tip Calculator app*:

| Unpopulated user interface | Populated user interface |
| :---: | :---: |
| ![Screenshot of the app's unpopulated interface](img/unpopulated.PNG) | ![Screenshot of the app's populated interface](img/populated.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 1 | Skill Set 2 | Skill Set 3 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 1 running](img/circle.PNG) | ![Screenshot of Skill Set 2 running](img/multiplenumber.PNG) | ![Screenshot of Skill Set 3 running](img/nestedstructures.PNG) |