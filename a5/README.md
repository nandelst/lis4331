# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Assignment 5 Requirements:

*Two Parts:*

1. News Reader Mobile App
2. Chapter Questions (Chs. 9, 10)
	
#### README.md file should include the following items:

* Screenshot of running application's items activity screen
* Screenshot of running application's item activity screen
* Screenshot of running application's read more... screen
* Screenshots of skill sets 13, 14, and 15

#### Assignment Screenshots:

*Screenshots of running application's items, item, and read more... screens*:

| Items Activity | Item Activity | Read More... |
| :---: | :---: | :---: |
| ![Screenshot of the app's items activity](img/itemsactivity.PNG) | ![Screenshot of the app's item activity](img/itemactivity.PNG) | ![Screenshot of the app's read more screen](img/readmore.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 13 | Skill Set 14 | Skill Set 15 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 13 running](img/WriteReadFile.PNG) | ![Screenshot of Skill Set 14 running](img/SimpleInterestCalculator.PNG) | ![Screenshot of Skill Set 15 running](img/ArrayDemo.PNG) |