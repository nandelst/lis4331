> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of running Java Hello
* Screenshot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App
* git commands w/ short descriptions

#### Git commands w/short descriptions:

1. git init - Initializes a new Git repository.
2. git status - Lets you see the status of your repository such as new changes or tracked files.
3. git add - Stages a specific file or your directory for updates.
4. git commit - Saves your updates to the local repository.
5. git push - Uploads your local repository to a remote one.
6. git pull - Downloads a remote repository and updates your local repository.
7. git tag - Adds a reference number to a specific file to mark a version of it.

#### Assignment Screenshots:

| Screenshot of running Java Hello | Screenshot of Android Studio - My First App |
| :---: | :---: |
| ![Screenshot of running Java Hello](img/jdk_install.PNG) | ![Screenshot of Android Studio - My First App](img/android.PNG) |

*Screenshots of Android Studio - Contacts App*:

| Screenshot of Contacts App - Main Screen | Screenshot of Contacts App - Info Screen |
| :---: | :---: |
| ![Screenshot of Contacts App - Main Screen](img/main.PNG) | ![Screenshot of Contacts App - Info Screen](img/info.PNG) |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nandelst/bitbucketstationlocations/ "Bitbucket Station Locations")
