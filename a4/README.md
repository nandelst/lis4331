# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Assignment 4 Requirements:

*Two Parts:*

1. Home Mortgage Mobile App
2. Chapter Questions (Chs. 11, 12)
	
#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's invalid screen
* Screenshots of running application's valid screen
* Screenshots of skill sets 10, 11, and 12

#### Assignment Screenshots:

*Screenshots of running application's splash and invalid screen*:

| Splash screen | Invalid screen |
| :---: | :---: |
| ![Screenshot of the app's splash screen](img/splash.PNG) | ![Screenshot of the app's invalid screen](img/invalid.PNG) |

*Screenshots of running application's valid screen*:

| Main screen | Valid Input | Valid Screen |
| :---: | :---: | :---: |
| ![Screenshot of the app's main screen](img/main.PNG) | ![Screenshot of valid input for the app](img/main2.PNG) | ![Screenshot of the app's valid screen](img/valid.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 10 | Skill Set 11 | Skill Set 12 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 10 running](img/TravelTime.PNG) | ![Screenshot of Skill Set 11 running](img/ProductDemo.PNG) | ![Screenshot of Skill Set 12 running](img/BookDemo.PNG) |