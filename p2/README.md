# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Project 2 Requirements:

*Two parts*:

1. My Users Mobile App
2. Project Questions

	
#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshots of using the "add user" feature of running application (at least 5 users)
* Screenshots of using the "update user" feature of running application
* Screenshots of using the "delete user" feature of running application


#### Assignment Screenshots:

*Screenshots of running application's splash screen and "add user" feature*:

| Splash screen | Add user | View users |
| :---: | :---: | :---: |
| ![Screenshot of the app's splash screen](img/splash.PNG) | ![Screenshot of the app's "add user" feature](img/add1.PNG) | ![Screenshot of the app's "view users" feature](img/add2.PNG) |

*Screenshots of running application's "update user" feature*:

| Update user | View users |
| :---: | :---: |
| ![Screenshot of the app's "update user" feature](img/update1.PNG) | ![Screenshot of the app's "view users" feature](img/update2.PNG) |

*Screenshots of running application's "delete user" feature*:

| Delete user | View users |
| :---: | :---: |
| ![Screenshot of the app's "delete user" feature](img/delete1.PNG) | ![Screenshot of the app's "view users" feature](img/delete2.PNG) |
