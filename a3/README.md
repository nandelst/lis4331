# LIS4331 - Advanced Mobile Application Development

## Douglas Nandelstadt

### Assignment 3 Requirements:

*Two Parts:*

1. Currency Converter Mobile App
2. Chapter Questions (Chs. 5, 6)
	
#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's unpopulated user interface
* Screenshot of running application's toast notification
* Screenshot of running application's converted currency user interface
* Screenshots of skill sets 4, 5, and 6

#### Assignment Screenshots:

*Screenshots of running application's splash screen and unpopulated user interface*:

| Splash screen | Unpopulated user interface |
| :---: | :---: |
| ![Screenshot of the app's splash screen](img/splash.PNG) | ![Screenshot of the app's unpopulated user interface](img/unpopulated.PNG) |

*Screenshots of running application's toast notification and converted currency user interface*:

| Toast notification | Converted currency user interface |
| :---: | :---: |
| ![Screenshot of the app's toast notification](img/toast.PNG) | ![Screenshot of the app's converted currency user interface](img/converted.PNG) |

*Screenshots of Skill Sets*:

| Skill Set 4 | Skill Set 5 | Skill Set 6 |
| :---: | :---: | :---: |
| ![Screenshot of Skill Set 4 running](img/TimeConversion.PNG) | ![Screenshot of Skill Set 5 running](img/EvenOrOdd.PNG) | ![Screenshot of Skill Set 6 running](img/PaintCalculator.PNG) |